#!/bin/bash
#FLASK_DEBUG=true
export FLASK_ENV=development
export FLASK_APP=banlist

if [[ "$1" == uwsgi ]]; then
	exec uwsgi --module $FLASK_APP:'create_app()' --http :5000
fi

exec flask run
