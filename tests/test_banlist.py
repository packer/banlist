import pytest
from time import sleep
from sqlalchemy.orm.exc import MultipleResultsFound

from tests.test import cleanup

from banlist.database import init_db, drop_db, db_session
from banlist.models import Ban
from banlist.views import bl


@pytest.fixture(scope='function')
def _init_db():
    init_db()


@pytest.fixture
def _drop_db(scope='function'):
    drop_db()


hostName = 'Test Server'
hostIP = '127.0.0.1'
bannedIP = '192.168.0.1'
duration = 3600
reason = 'Some unknwon reason.'


def test_addBan(_drop_db, _init_db):
    bl.addBan(hostName, hostIP, bannedIP, duration, reason)
    ban = db_session.query(Ban).filter_by(
             hostIP=hostIP, bannedIP=bannedIP).first()
    assert ban is not None
    # add the same ban a second time to test update
    bl.addBan(hostName, hostIP, bannedIP, duration, reason)
    # check if the ban exists twice...
    try:
        db_session.query(Ban).filter_by(
            hostIP=hostIP, bannedIP=bannedIP).one()
    except MultipleResultsFound:
        raise


def test_removeBan(_drop_db, _init_db):
    bl.removeBan(hostIP, bannedIP)
    bl.addBan(hostName, hostIP, bannedIP, duration, reason)
    bl.removeBan(hostIP, bannedIP)
    ban = db_session.query(Ban).filter_by(
            hostIP=hostIP, bannedIP=bannedIP).first()
    assert ban is None


def test_listBansFor(_drop_db, _init_db):
    bans = bl.listBansFor(hostIP, hostIP)
    assert not bans
    bl.addBan(hostName, hostIP, '192.168.0.1', duration, reason)
    bl.addBan(hostName, hostIP, '192.168.0.2', duration, reason)
    bans = bl.listBansFor(hostIP, hostIP)
    assert isinstance(bans, str)


def test_others(_drop_db, _init_db):
    # bl.refresh and bl.getAllBans
    bl.refresh()
    bl.addBan(hostName, hostIP, '192.168.0.1', 1, reason)
    bl.addBan(hostName, hostIP, '192.168.0.2', 1, reason)

    bans = bl.getAllBans()
    assert bans
    assert str(bans[0]) != str(bans[1])

    sleep(1.5)
    bl.refresh()

    bans = bl.getAllBans()
    assert not bans
