from banlist import create_app
from banlist.views import uts_to_date, strip_qstring, seconds_to_time

from tests.test import cleanup


def test_filters():
    uts = 1853172703.7141955
    date = uts_to_date(uts)
    assert date == '2028-09-21 18:11:43'

    uts = 99999999999999999999999999999999999999.999999999999
    date = uts_to_date(uts)
    assert date == 'Far in the future'

    qstr = "^1 ^2 ^3 ^4^xF0F!"
    result = strip_qstring(qstr)
    assert result == 'This is my NAME!'

    seconds = 15
    t = seconds_to_time(seconds)
    assert t == '00h 00m 15s'

    seconds = 32*60+37
    t = seconds_to_time(seconds)
    assert t == '00h 32m 37s'

    seconds = 12*3600+11*60+19
    t = seconds_to_time(seconds)
    assert t == '12h 11m 19s'

    seconds = 18*24*3600+12*3600+11*60+19
    t = seconds_to_time(seconds)
    assert t == '18d 12h 11m 19s'


def test_views():
    app = create_app()

    with app.test_client() as c:
        # invalid request
        r = c.get('/?action=xxx')
        assert r.status == '400 BAD REQUEST'
        assert r.mimetype == 'text/plain'

        # submit a ban
        r = c.get('/?action=ban&ip=192.168.0.1')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # submit removing a ban
        r = c.get('/?action=unban&ip=192.168.0.1')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # query empty ban list
        r = c.get('/?action=list&servers=127.0.0.1')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'
        assert r.data == b''

        # submit another ban
        r = c.get('/?action=ban&ip=192.168.0.2')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # query banlist with one entry
        r = c.get('/?action=list&servers=127.0.0.1')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'
        assert r.data.decode('utf-8').count('\n') == 4

        # submit another ban
        r = c.get('/?action=ban&ip=192.168.0.3')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # query banlist with two entries
        r = c.get('/?action=list&servers=127.0.0.1;xxx')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'
        assert r.data.decode('utf-8').count('\n') == 8

        # submit ban with absurd duration
        r = c.get('/?action=ban&ip=192.168.0.4&duration=999999999999999999999999999999999999999999.9999999')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # submit ban with inf duration
        r = c.get('/?action=ban&ip=192.168.0.5&duration=inf')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/plain'

        # pull the website in html format
        r = c.get('/')
        assert r.status == '200 OK'
        assert r.mimetype == 'text/html'

        app.teardown_appcontext(IOError)
