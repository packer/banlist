from banlist.util import qfont_to_ascii, strip_colors, strip_qstring


def test_strings():
    qstr = "^1 ^2 ^3 ^4^xF0F!"
    result = strip_qstring(qstr)
    assert result == 'This is my NAME!'

    qstr = None
    result = strip_qstring(qstr)
    assert result == ''

    qstr = None
    result = qfont_to_ascii(qstr)
    assert result == ''

    qstr = '^xF00^7x'
    res = strip_colors(qstr)
    assert res == 'x'

    qstr = None
    res = strip_colors(qstr)
    assert res == ''
