from banlist.models import Ban
from banlist.time import itime


def test_newBan():
    ts = itime()
    ban = Ban(hostName='HostName',
              hostIP='HostIP',
              bannedIP='BannedIP',
              reason='reason',
              bannedAt=ts,
              bannedUntil=ts + 3600)

    assert ban.hostName == 'HostName'
    assert ban.hostIP == 'HostIP'
    assert ban.bannedIP == 'BannedIP'
    assert ban.reason == 'reason'
    assert ban.bannedAt is not None
    assert ban.bannedUntil == ts + 3600
