# Xonotic Banlist

A Flask application to provide a ban list for [Xonotic](https://xonotic.org/).
The protocol of the ban list is available [here](https://gitlab.com/xonotic/xonotic-data.pk3dir/-/blob/master/qcsrc/server/ipban.qc).

# Setup

Clone the repository:

```
git clone https://gitlab.com/packer/banlist.git ~/banlist
```

## virtualenv

```
cd ~/banlist/
mkdir env/
virtualenv env/
source env/bin/activate
pip install -r requirements.txt
```

## pipenv

```
cd ~/banlist/
pipenv install
```

# Run

```
# Production
./uwsgi.sh
# Development
./development.sh
```

It is advised to use `uwsgi` (`./uwsgi.sh`) for a production environment. The `uwsgi`
environment uses a socket by default. As alternative you can use `./run.py`.
For development purposes use `./development.sh` or `./development.sh uwsgi`.


## systemd

The given examples are for a systemd service running under a specific user.
To run the applcication as a system user the service file and `uwsgi.ini` have
to be adjusted.

### virtualenv

```
[Unit]
Description=Banlist

[Service]
PIDFile=%h/banlist/uwsgi.pid
ExecStart=%h/banlist/env/bin/uwsgi --ini=uwsgi.ini
WorkingDirectory=%h/banlist/
RuntimeDirectory=uwsgi
Restart=always
RestartSec=5

ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill -INT $MAINPID
Type=notify
NotifyAccess=all
KillSignal=SIGQUIT

[Install]
WantedBy=default.target
```

### pipenv

```
[Unit]
Description=Banlist

[Service]
PIDFile=%h/banlist/uwsgi.pid
ExecStart=/usr/bin/pipenv run uwsgi --ini=uwsgi.ini
WorkingDirectory=%h/banlist/
RuntimeDirectory=uwsgi
Restart=always
RestartSec=5

ExecReload=/bin/kill -HUP $MAINPID
ExecStop=/bin/kill -INT $MAINPID
Type=notify
NotifyAccess=all
KillSignal=SIGQUIT

[Install]
WantedBy=default.target
```


It is advised to use nginx as front-end reverse proxy!

## NGINX

```
location /banlist/some-random-string/ {
	rewrite /banlist/[^/]+/ / break;
	include uwsgi_params;
	uwsgi_pass unix:/custom-path/banlist/banlist.sock;
}

```

## Xonotic

Finally set the following cvars in your Xonotic server:

```
g_ban_sync_uri "http://example.org/custom-uri/"
g_ban_sync_trusted_servers "list of server ips to share bans with"
```

Note: To preserve bans across server restarts at the servers own IP to the list
      of trusted servers.
