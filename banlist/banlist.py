import logging
from .database import db_session
from .models import Ban
from .time import itime

module_logger = logging.getLogger(__name__)


class Banlist(object):
    def refresh():
        """ Remove vanished bans from DB. """
        module_logger.info("Refreshing banlist...")
        bans = db_session.query(Ban).filter(
            Ban.bannedUntil <= itime()).all()
        if bans:
            db_session.query(Ban).filter(
                Ban.bannedUntil <= itime()).delete()
            for ban in bans:
                module_logger.info(
                    "Removed ban {} on server {}.".format(
                        ban.bannedIP, ban.hostIP))
            db_session.commit()

        return

    def addBan(hostName, hostIP, bannedIP, duration, reason):
        """ Update an existing or add a new ban. """

        duration = min(float(duration), 3155760000)  # ~100 years
        bannedAt = itime()
        bannedUntil = bannedAt + duration

        updated = db_session.query(Ban).filter_by(
            hostIP=hostIP, bannedIP=bannedIP).update(
            {'bannedUntil': bannedUntil})

        if updated:
            module_logger.info(
                "Updated ban {} on server {}.".format(
                    bannedIP, hostIP))
        else:
            ban = Ban(hostName=hostName,
                      hostIP=hostIP,
                      bannedIP=bannedIP,
                      reason=reason,
                      bannedAt=bannedAt,
                      bannedUntil=bannedUntil)
            db_session.add(ban)
            module_logger.info(
                "Added ban {} on server {}.".format(
                   ban.bannedIP, ban.hostIP))

        db_session.commit()
        return

    def removeBan(hostIP, bannedIP):
        """ Remove a ban. """
        deleted = db_session.query(Ban).filter_by(
            hostIP=hostIP, bannedIP=bannedIP).delete()

        if deleted:
            db_session.commit()
            module_logger.info(
                "Removed ban {} on server {}.".format(
                    bannedIP, hostIP))

        return

    def getAllBans():
        """ Return all bans for webpage. """
        bans = db_session.query(Ban).order_by(Ban.bannedAt).all()
        return bans

    def listBansFor(hostIP, servers):
        """ Return bans in Xonotic required format """
        servers = servers.split(';')
        bans = db_session.query(Ban).filter(Ban.hostIP.in_(servers)).all()

        reply = ''
        c = 0
        ts = itime()
        for ban in bans:
            reply = ''.join([reply, "{}\n{}\n{}\n{}\n".format(
                ban.bannedIP,
                ban.bannedUntil - ts,
                ban.reason,
                ban.hostIP)])
            c += 1

        module_logger.info("Sent {} bans for servers {} to {}.".format(
                c, servers, hostIP))
        return reply
