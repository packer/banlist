from flask import render_template, request, Response, Blueprint
from datetime import datetime

from .time import itime
from .banlist import Banlist as bl
from .util import strip_qstring as _strip_qstring

views = Blueprint('views', __name__, template_folder='templates')


@views.app_template_filter('date')
def uts_to_date(uts):
    try:
        return str(datetime.utcfromtimestamp(int(uts)))
    except Exception as e:
        return "Far in the future"


@views.app_template_filter('strip_qstring')
def strip_qstring(qstr=''):
    return _strip_qstring(qstr)


@views.app_template_filter('seconds_to_time')
def seconds_to_time(sec):
    sec = int(sec)
    m, s = divmod(sec, 60)
    h, m = divmod(m, 60)
    d, h = divmod(h, 24)
    if d > 0:
        return "{}d {:0>2d}h {:0>2d}m {:0>2d}s".format(d, h, m, s)

    return "{:0>2d}h {:0>2d}m {:0>2d}s".format(h, m, s)


@views.route("/")
def index():

    bl.refresh()
    action = request.args.get('action')

    if(action is not None):
        hostName = request.args.get('hostname', "No hostname set")
        hostIP = request.environ['REMOTE_ADDR']

        if (action == 'list'):
            servers = request.args.get('servers')
            if servers is not None:
                return Response(
                    bl.listBansFor(hostIP, servers),
                    status=200, mimetype='text/plain')

        elif (action == 'ban'):
            bannedIP = request.args.get('ip')
            duration = request.args.get('duration', 3600, type=float)
            # type=float, integer is too small :)
            reason = request.args.get('reason', 'No reason provided')
            if bannedIP is not None:
                bl.addBan(hostName, hostIP, bannedIP, duration, reason)
                return Response(
                    '', status=200, mimetype='text/plain')

        elif (action == 'unban'):
            bannedIP = request.args.get('ip')
            if bannedIP is not None:
                bl.removeBan(hostIP, bannedIP)
                return Response(
                    '', status=200, mimetype='text/plain')

        return Response(
            'Bad Request.\n', status=400, mimetype='text/plain')

    return render_template("banlist.html", now=itime(), banlist=bl.getAllBans())
