from flask import Flask
import logging
from .database import db_session, init_db


def create_app():

    FORMAT = '[%(asctime)s] %(name)s %(levelname)s: %(message)s'
    logging.basicConfig(format=FORMAT, level='WARNING')

    app = Flask(__name__)

    with app.app_context():
        init_db()

    @app.teardown_appcontext
    def shutdown_session(exception=None):
        db_session.remove()

    from .views import views
    app.register_blueprint(views)

    return app
