from sqlalchemy import Column, String, Float, UniqueConstraint
from .database import Base


class Ban(Base):
    __tablename__ = 'bans'
    hostName = Column(String(128), nullable=False)
    hostIP = Column(String(39), nullable=False, primary_key=True)
    # bannedIP can be a fingerprint, therefore 45 chars.
    bannedIP = Column(String(45), nullable=False, primary_key=True)
    reason = Column(String(255), nullable=False)
    bannedAt = Column(Float, nullable=False, index=True)
    bannedUntil = Column(Float, nullable=False)
    __table_args__ = (UniqueConstraint('hostIP', 'bannedIP'), )

    def __repr__(self):
        return "<BAN: {}, {}, {}>".format(
            self.hostName,
            self.hostIP,
            self.bannedIP)
